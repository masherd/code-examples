#include "networkpackets.h"

void UDPPacket::parse_data() {
    char *pack = data.Data();
    int type;
    if(data.Size() < 3)
    {
        type = 1;
    }

    if (pack[0] == char(250) && pack[1] == char(243) && pack[2] == char(32))
        type = 2;
    else
        type = 1;

    if (type == 2)
    {
        unsigned char byte;
        byte = *(unsigned char*) (pack + 5);
        outer_chan = (byte >> 4) & 7;
    }
    if (type == 1)
    {
        pack += 4;
        outer_chan = *(unsigned char *) pack;
    }

    int package_size = data.Size();
    for(int i = 0; i < package_size; i += 2) {
        unsigned char secondByte = *(unsigned char*)(pack + i + 1);
        char r13 = (secondByte >> 5) & 1;
        char r12 = (secondByte >> 4) & 1;
        int inner_chan = (r12 << 1) + r13;
        inner_chans.insert(inner_chan);
    }
}

UDPPacket::UDPPacket(const char *src, timeval ts) : time(ts) {
    src_port = ntohs(*(const uint16_t *)src);
    src += 2;
    dst_port = ntohs(*(const uint16_t *)src);
    src += 2;
    uint16_t len = ntohs(*(const uint16_t *)src);
    src += 2;
    checksum = ntohs(*(const uint16_t *)src);
    src += 2;
    len -= sizeof(uint16_t) * 4;
    data = DataStructure(src, len);

    parse_data();
}

UDPPacket::UDPPacket(const char *src, int len) {
    data = DataStructure(src, len);
    time = {0, 0};
    parse_data();
}

uint16_t UDPPacket::GetSrcPort() const {
    return src_port;
}

uint16_t UDPPacket::GetDestPort() const {
    return dst_port;
}

uint16_t UDPPacket::GetChecksum() const {
    return checksum;
}

int UDPPacket::GetOuterChan() const {
    return outer_chan;
}

const std::set<int> &UDPPacket::GetInnerChans() const {
    return inner_chans;
}

const DataStructure &UDPPacket::GetData() const {
    return data;
}

const timeval &UDPPacket::GetTime() const {
    return time;
}

std::ostream &operator<<(std::ostream &stream, const UDPPacket &ob) {
    QDateTime time;
    time.setTime_t(ob.GetTime().tv_sec);
    time = time.addMSecs(ob.GetTime().tv_usec / 1000);
    stream << "src_port: " << ob.GetSrcPort() << ", dst_port: " << ob.GetDestPort() << ", time:"
           << time.time().toString("hh:mm:ss:z").toStdString() << ", checksum: " << ob.GetChecksum()
           << ", data: " << ob.GetData() << ", out_chan: " << ob.GetOuterChan() << ", inner_chans:";
    auto s = ob.GetInnerChans();
    for (auto it = s.begin(); it != s.end(); ++it) {
        stream << " " << *it;
    }
    stream << ";";
    return stream;
}
