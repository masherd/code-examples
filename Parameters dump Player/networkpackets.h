#ifndef NETWORKPACKETS_H
#define NETWORKPACKETS_H

extern "C" {
#include "Winsock2.h"
}
#include "mvalue.h"
#include <ostream>
#include <set>
#include <QTime>

class UDPPacket {
private:
    uint16_t src_port;
    uint16_t dst_port;
    uint16_t checksum;
    int outer_chan;
    std::set<int> inner_chans;
    DataStructure data;
    struct timeval time;

    void parse_data();

public:
    UDPPacket(const char *src, struct timeval ts);
    UDPPacket(const char *src, int len);

    uint16_t GetSrcPort() const;
    uint16_t GetDestPort() const;
    uint16_t GetChecksum() const;
    int GetOuterChan() const;
    const std::set<int> &GetInnerChans() const;
    const DataStructure &GetData() const;
    const struct timeval &GetTime() const;
};

std::ostream &operator<<(std::ostream &stream, const UDPPacket &ob);

#endif // NETWORKPACKETS_H
