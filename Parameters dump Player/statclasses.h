#ifndef STATCLASSES_H
#define STATCLASSES_H
#include <QVector>
#include <QPair>
#include <QMap>
#include <QSet>
#include <ostream>
#include <QDataStream>
#include <QDateTime>
#include <sys/time.h>

struct Percent {
    int value;
    QDateTime time;
    bool operator==(const Percent &ob) const;
    bool operator!=(const Percent &ob) const;
    bool operator<(const Percent &ob) const;
    bool operator>(const Percent &ob) const;
    bool operator<=(const Percent &ob) const;
    bool operator>=(const Percent &ob) const;
    Percent &operator=(const Percent &ob);
    int operator-(const Percent &ob) const;
    std::string ToString() const;
    Percent(int val_ = -1, const QDateTime &time_ = QDateTime());
    Percent(int val_, const struct timeval &time_);
};

namespace Convert {
    std::string ToString(const Percent &ob);
}

uint qHash(const Percent &ob);

QDataStream &operator>>(QDataStream &ds, Percent &obj);

QDataStream &operator<<(QDataStream &ds, const Percent &obj);

std::ostream &operator<<(std::ostream &stream, const Percent &ob);

typedef QSet<Percent> PercentsVector;

QVector<QPair<Percent, Percent> > make_distances(const PercentsVector &numbers);

QString distances_to_string(const QVector<QPair<Percent, Percent> > &ob);

//~~~~~~~~~~~~~~~~ S T A T   C L A S S ~~~~~~~~~~~~~~~~

class StatClass {
protected:
    int number;
    PercentsVector percents;
    bool is_active;

public:
    StatClass(int number_);
    int GetNumber() const;
    const PercentsVector &GetPercents() const;
    PercentsVector &GetPercents();
    void AddPercent(Percent percent);
};

QDataStream &operator<<(QDataStream &ds, const StatClass &obj);
QDataStream &operator>>(QDataStream &ds, StatClass &obj) ;

//~~~~~~~~~~~~~~~~ S T A T   I N N E R   C H A N ~~~~~~~~~~~~~~~~

class StatInnerChan : public StatClass {
private:

public:
    StatInnerChan(int number_ = -1);
};

std::ostream &operator<<(std::ostream &stream, const StatInnerChan &ob);
QDataStream &operator<<(QDataStream &ds, const StatInnerChan &obj);
QDataStream &operator>>(QDataStream &ds, StatInnerChan &obj);

//~~~~~~~~~~~~~~~~ S T A T   O U T E R   C H A N ~~~~~~~~~~~~~~~~

class StatOuterChan : public StatClass {
private:
    QMap<int, StatInnerChan> inner_chans;

public:
    StatOuterChan(int number_ = -1);
    const QMap<int, StatInnerChan> &GetInnerChans() const;
    QMap<int, StatInnerChan> &GetInnerChans();
    void AddInnerChan(const StatInnerChan &chan);
};

std::ostream &operator<<(std::ostream &stream, const StatOuterChan &ob);
QDataStream &operator<<(QDataStream &ds, const StatOuterChan &obj);
QDataStream &operator>>(QDataStream &ds, StatOuterChan &obj);

//~~~~~~~~~~~~~~~~ S T A T   P O R T ~~~~~~~~~~~~~~~~

class StatPort : public StatClass {
private:
    QMap<int, StatOuterChan> outer_chans;

public:
    StatPort(int port = -1);
    const QMap<int, StatOuterChan> &GetOuterChans() const;
    QMap<int, StatOuterChan> &GetOuterChans();
    void AddOuterChan(const StatOuterChan &out_chan);
};

std::ostream &operator<<(std::ostream &stream, const StatPort &ob);
QDataStream &operator<<(QDataStream &ds, const StatPort &obj);
QDataStream &operator>>(QDataStream &ds, StatPort &obj);

#endif // STATCLASSES_H
