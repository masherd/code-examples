#include "statclasses.h"

bool Percent::operator==(const Percent &ob) const { return value == ob.value; }
bool Percent::operator!=(const Percent &ob) const { return value != ob.value; }
bool Percent::operator<(const Percent &ob) const { return value < ob.value; }
bool Percent::operator>(const Percent &ob) const { return value > ob.value; }
bool Percent::operator<=(const Percent &ob) const { return value <= ob.value; }
bool Percent::operator>=(const Percent &ob) const { return value >= ob.value; }
Percent &Percent::operator=(const Percent &ob) {
    value = ob.value;
    time = ob.time;
}
int Percent::operator-(const Percent &ob) const {
    return value - ob.value;
}

Percent::Percent(int val_, const QDateTime &time_) : value(val_), time(time_) { }

Percent::Percent(int val_, const struct timeval &time_) : value(val_) {
    time.setTime_t(time_.tv_sec);
    time = time.addMSecs(time_.tv_usec);
}

namespace Convert {
    std::string ToString(const Percent &ob) {
        return ob.time.toString("hh:mm:ss:z").toStdString() + "=" + std::to_string(ob.value) + "%";
    }
}

uint qHash(const Percent &ob) {
    return qHash(ob.value);
}

QDataStream &operator>>(QDataStream &ds, Percent &obj) {
    int number;
    QDateTime time;
    ds >> number >> time;
    obj.value = number;
    obj.time = time;
    return ds;
}

QDataStream &operator<<(QDataStream &ds, const Percent &obj) {
    ds << obj.value << obj.time;
    return ds;
}

std::ostream &operator<<(std::ostream &stream, const Percent &ob) {
    stream << ob.time.toString("hh:mm:ss:z").toStdString() << "=" << ob.value;
    return stream;
}

QVector<QPair<Percent, Percent> > make_distances(const QSet<Percent> &numbers) {
    QVector<Percent> nums;
    for (auto it = numbers.begin(); it != numbers.end(); ++it) {
        nums.push_back(*it);
    }
    qSort(nums.begin(), nums.end());
    QVector<QPair<Percent, Percent> > res;
    QPair<Percent, Percent> cur;
    cur.first = -1;
    cur.second = -1;
    Percent prev = nums[0];
    for (int i = 0, size = nums.size(); i < size; ++i) {
        if (cur.first < 0) {
            cur.first = nums[i];
        } else if ((nums[i] - prev) > 1) {
            cur.second = prev;
            res.push_back(cur);
            cur.first = nums[i];
            cur.second = -1;
        }
        prev = nums[i];
    }
    if (cur.second < 0) {
        cur.second = nums.back();
        res.push_back(cur);
    }
    return res;
}

QString distances_to_string(const QVector<QPair<Percent, Percent> > &ob) {
    QString res;
    int i = 0, sz = ob.size();
    for (auto it = ob.begin(); it != ob.end(); ++it, ++i) {
        res += QString::fromStdString(Convert::ToString((*it).first) + " - " + Convert::ToString((*it).second));
        if (i + 1 < sz) res += "; ";
    }
    return res;
}

//~~~~~~~~~~~~~~~~ S T A T   C L A S S ~~~~~~~~~~~~~~~~

StatClass::StatClass(int number_) : number(number_), is_active(true) { }

int StatClass::GetNumber() const {
    return number;
}

const PercentsVector &StatClass::GetPercents() const {
    return percents;
}

PercentsVector &StatClass::GetPercents() {
    return percents;
}

void StatClass::AddPercent(Percent percent) {
    percents.insert(percent);
}

QDataStream &operator<<(QDataStream &ds, const StatClass &obj) {
    ds << obj.GetNumber() << obj.GetPercents();
    return ds;
}

QDataStream &operator>>(QDataStream &ds, StatClass &obj) {
    int number;
    PercentsVector vec;
    ds >> number >> vec;
    obj = StatClass(number);
    obj.GetPercents() = vec;
    return ds;
}

//~~~~~~~~~~~~~~~~ S T A T   I N N E R   C H A N ~~~~~~~~~~~~~~~~

StatInnerChan::StatInnerChan(int number_) : StatClass(number_) { }

std::ostream &operator<<(std::ostream &stream, const StatInnerChan &ob) {
    stream << "StatInnerChan: number=" << ob.GetNumber() << ", percents=";
    for (auto it = ob.GetPercents().begin(); it != ob.GetPercents().end(); ++it) {
        stream << " " << *it;
    }
    return stream;
}

QDataStream &operator<<(QDataStream &ds, const StatInnerChan &obj) {
    ds << ((const StatClass &)obj);
    return ds;
}

QDataStream &operator>>(QDataStream &ds, StatInnerChan &obj) {
    ds >> ((StatClass &)obj);
    return ds;
}

//~~~~~~~~~~~~~~~~ S T A T   O U T E R   C H A N ~~~~~~~~~~~~~~~~

StatOuterChan::StatOuterChan(int number_) : StatClass(number_) { }

const QMap<int, StatInnerChan> &StatOuterChan::GetInnerChans() const {
    return inner_chans;
}

QMap<int, StatInnerChan> &StatOuterChan::GetInnerChans() {
    return inner_chans;
}

void StatOuterChan::AddInnerChan(const StatInnerChan &chan) {
    inner_chans.insert(chan.GetNumber(), chan);
}

std::ostream &operator<<(std::ostream &stream, const StatOuterChan &ob) {
    stream << "StatOuterChan: number=" << ob.GetNumber() << ", percents=";
    for (auto it = ob.GetPercents().begin(); it != ob.GetPercents().end(); ++it) {
        stream << " " << *it;
    }
    stream << std::endl << "inner_chans=";
    for (auto it = ob.GetInnerChans().begin(); it != ob.GetInnerChans().end(); ++it) {
        stream << *it << std::endl;
    }
    return stream;
}

QDataStream &operator<<(QDataStream &ds, const StatOuterChan &obj) {
    ds << ((const StatClass &)obj) << obj.GetInnerChans();
    return ds;
}

QDataStream &operator>>(QDataStream &ds, StatOuterChan &obj) {
    QMap<int, StatInnerChan> inner_chans;
    ds >> ((StatClass &)obj) >> inner_chans;
    obj.GetInnerChans() = inner_chans;
    return ds;
}

//~~~~~~~~~~~~~~~~ S T A T   P O R T ~~~~~~~~~~~~~~~~

StatPort::StatPort(int port) : StatClass(port) { }

const QMap<int, StatOuterChan> &StatPort::GetOuterChans() const {
    return outer_chans;
}

QMap<int, StatOuterChan> &StatPort::GetOuterChans() {
    return outer_chans;
}

void StatPort::AddOuterChan(const StatOuterChan &out_chan) {
    outer_chans.insert(out_chan.GetNumber(), out_chan);
}

std::ostream &operator<<(std::ostream &stream, const StatPort &ob) {
    stream << "StatPort: number=" << ob.GetNumber() << ", percents=";
    for (auto it = ob.GetPercents().begin(); it != ob.GetPercents().end(); ++it) {
        stream << " " << *it;
    }
    stream << std::endl << "outer_chans=";
    for (auto it = ob.GetOuterChans().begin(); it != ob.GetOuterChans().end(); ++it) {
        stream << *it << std::endl;
    }
    return stream;
}

QDataStream &operator<<(QDataStream &ds, const StatPort &obj) {
    ds << ((const StatClass &)obj) << obj.GetOuterChans();
    return ds;
}

QDataStream &operator>>(QDataStream &ds, StatPort &obj) {
    QMap<int, StatOuterChan> outer_chans;
    ds >> ((StatClass &)obj) >> outer_chans;
    obj.GetOuterChans() = outer_chans;
    return ds;
}
