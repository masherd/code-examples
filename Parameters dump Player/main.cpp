#include "windowlgmisplayer.h"
#include <QApplication>
#define HAVE_REMOTE
#include "custom_pcap.h"
#include "logger.h"

int main(int argc, char *argv[])
{
    Logger::TurnOffLogType(LT_DEBUG);
    QApplication a(argc, argv);
    WindowLGMISPlayer w;
    w.show();
    return a.exec();
}
