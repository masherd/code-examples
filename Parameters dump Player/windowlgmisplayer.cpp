#include "windowlgmisplayer.h"
#include "ui_windowlgmisplayer.h"
#include "custom_pcap.h"
#include "logger.h"
#include "networkpackets.h"

#include <iostream>
#include <fstream>
#include <QFileDialog>
#include <QCheckBox>
#include <QPlainTextEdit>
#include <QListWidget>
#define USEC_PER_SEC 1000000

//~~~~~~~~~~~~~~~~~~~~~~~~ W A I T  F O R ~~~~~~~~~~~~~~~~~~~~~~~~

wait_for::wait_for() : wait_please(false), continue_please(true), is_waiting(false) {
    mt.reset(new QMutex());
}

bool wait_for::start_wait() {
    QMutexLocker locker(mt.get());
    if (!wait_please) return false;
    is_waiting = true;
    continue_please = false;
    while (!continue_please) { QThread::currentThread()->usleep(100); };
    is_waiting = false;
    wait_please = false;
    return true;
}

bool wait_for::make_waiting(bool block) {
    bool old = wait_please;
    mt->lock();
    wait_please = true;
    mt->unlock();
    if (block) while (!is_waiting) { QThread::currentThread()->usleep(100); }
    LogFFL(LT_DEBUG) << "\n";
    return old;
}

bool wait_for::stop_waiting() {
    continue_please = true;
    wait_please = false;
    return true;
}

//~~~~~~~~~~~~~~~~~~~~~~~~ P C A P   T H R E A D ~~~~~~~~~~~~~~~~~~~~~~~~

PCAPThread::PCAPThread(WindowLGMISPlayer *_player) : player(_player), socket(nullptr), terminate(false), wasted(false)
{
    waits.insert(std::make_pair(std::string("waste"), wait_for()));
    waits.insert(std::make_pair(std::string("pause"), wait_for()));
}

void PCAPThread::run() {
    if (socket != nullptr) {
        delete socket;
        socket = nullptr;
    }
    if (!player->in_stat_making) socket = new QUdpSocket();
    struct pcap_pkthdr *header;
    const u_char *data;

    DataStructure data_buffer;
    data_buffer.Resize(2000);
    struct pcap_pkthdr header_buffer;

    int res;
    while ((res = pcap_next_ex(this->player->fp, &header, &data)) >= 0) {
        header_buffer = *header;
        if (header_buffer.caplen >= data_buffer.Size()) {
            data_buffer.Resize(header_buffer.caplen + 100);
        }
        memcpy(data_buffer.Data(), data, header_buffer.caplen);
        while(1) {
            bool ready = true;
            for (auto it = waits.begin(); it != waits.end(); ++it) {
                if (it->second.start_wait()) ready = false;
            }
            if (ready) break;
        }

        if (!res) {
            LogF(LT_DEBUG) << "timeout elapsed\n";
            return;
        }
        if (terminate) {
            LogF(LT_DEBUG) << "terminate signal from player\n";
            return;
        }
        const struct sniff_ip *ip;
        const struct sniff_udp *udp;
        int size_ip;

        ip = (struct sniff_ip*)data_buffer.Data(SIZE_ETHERNET);
        size_ip = IP_HL(ip) * 4;
        switch(ip->ip_p) {
            case IPPROTO_TCP:
                return;
            case IPPROTO_UDP:
                break;
            case IPPROTO_ICMP:
                return;
            case IPPROTO_IP:
                return;
            default:
                return;
        }
        udp = (struct sniff_udp*)data_buffer.Data(SIZE_ETHERNET + size_ip);
        UDPPacket packet((const char *)udp, header->ts);
        LogF(LT_DEBUG) << packet << "\n";
        int usecs = 0;
        bool is_filtered_out = false;
        if (!player->in_stat_making) {
            int port = packet.GetDestPort();
            int out_chan = packet.GetOuterChan();
            if (player->port_to_gui.contains(port)) {
                GUIStat &port_stat = player->port_to_gui[port];
                if (port_stat.check->isChecked()) {
                    if (port_stat.childs.contains(out_chan)) {
                        GUIStat &out_stat = port_stat.childs[out_chan];
                        if (!out_stat.check->isChecked())
                            is_filtered_out = true;
                    }
                } else {
                    is_filtered_out = true;
                }
            }
        }
        if (player->packets_iterator > 0) {
            usecs = header_buffer.ts.tv_sec * USEC_PER_SEC + header_buffer.ts.tv_usec - player->prev_packet.tv_sec * USEC_PER_SEC - player->prev_packet.tv_usec;
            usecs = (usecs * 1.0 / player->speed_coef);
            if (!wasted && (usecs > 0) && (!player->in_stat_making) && (!is_filtered_out)) usleep(usecs);
        }
        if (!wasted && (usecs >= 0)) player->prev_packet = header_buffer.ts;
        if (wasted) wasted = false;
        if ((!player->in_stat_making) && (!is_filtered_out)) {
            socket->writeDatagram(packet.GetData().Data(), packet.GetData().Size(), addr, packet.GetDestPort());
        }
        player->packets_iterator++;
        int percent = (player->packets_iterator * 100 / player->packets_count);

        //-------- S T A T --------
        if (player->in_stat_making) {
            int port = packet.GetDestPort();
            StatPort *stat_port = nullptr;
            if (!player->stats.contains(port)) {
                StatPort new_stat(port);
                player->stats.insert(port, new_stat);
            }
            stat_port = &(player->stats[port]);
            stat_port->AddPercent(Percent(percent, packet.GetTime()));
            int out_chan = packet.GetOuterChan();
            StatOuterChan *stat_out = nullptr;
            if (!stat_port->GetOuterChans().contains(out_chan)) {
                StatOuterChan new_out_chan(out_chan);
                stat_port->AddOuterChan(new_out_chan);
            }
            stat_out = &(stat_port->GetOuterChans()[out_chan]);
            stat_out->AddPercent(Percent(percent, packet.GetTime()));
            auto inner_chans = packet.GetInnerChans();
            for (auto it = inner_chans.begin(); it != inner_chans.end(); ++it) {
                if (!stat_out->GetInnerChans().contains(*it)) {
                    StatInnerChan new_inn_chan(*it);
                    stat_out->AddInnerChan(new_inn_chan);
                }
                StatInnerChan *stat_inn = &(stat_out->GetInnerChans()[*it]);
                stat_inn->AddPercent(Percent(percent, packet.GetTime()));
            }
        }

        emit progress_changed(percent);
    }
    LogF(LT_DEBUG) << "end of sending\n";
    exit(0);
}

//~~~~~~~~~~~~~~~~~~~~~~~~ G U I   S T A T ~~~~~~~~~~~~~~~~~~~~~~~~

GUIStat::GUIStat() : number(-1), expand(nullptr), check(nullptr), type(GUIStat::GUI_UNDEF) { }

GUIStat::GUIStat(int number_, QPushButton *expand_, QCheckBox *check_, GUIStatType type_)
    : number(number_), expand(expand_), check(check_), type(type_) { }

std::ostream &operator<<(std::ostream &stream, const GUIStat &ob) {
    stream << "number: " << ob.number << ", type: " << ob.type << ", childs:\n";
    for (auto it = ob.childs.begin(); it != ob.childs.end(); ++it) {
        stream << *it << std::endl;
    }
    return stream;
}

//~~~~~~~~~~~~~~~~~~~~~~~~ W I N D O W   L G M I S   P L A Y E R ~~~~~~~~~~~~~~~~~~~~~~~~

WindowLGMISPlayer::WindowLGMISPlayer(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::WindowLGMISPlayer), packets_count(-1), packets_iterator(0), speed_coef(1),
    in_stat_making(false), state(WindowLGMISPlayer::STOPED), worker(nullptr), fp(NULL)
{
    ui->setupUi(this);
    ui->table_stat->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    //ui->table_stat->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

void WindowLGMISPlayer::open_fp()
{
    fp = pcap_open(source, 65536, PCAP_OPENFLAG_PROMISCUOUS, 1000, NULL, errbuf);
}

void WindowLGMISPlayer::on_button_expand_port_clicked() {
    QPushButton *button_port = (QPushButton *)sender();
    int port_number = buttons_port_to_num[button_port];
    GUIStat &port_stat = port_to_gui[port_number];
    QTableWidget *table = ui->table_stat;
    int out_row = -1;
    for (int i = 0, sz = table->rowCount(); i < sz; ++i) {
        if (port_stat.expand == table->cellWidget(i, 0)) {
            out_row = i;
            break;
        }
    }
    out_row++;
    if (port_stat.expand->text() == "+") {
        table->showRow(out_row);
        port_stat.expand->setText("-");
    } else {
        table->hideRow(out_row);
        port_stat.expand->setText("+");
    }
}

void WindowLGMISPlayer::on_button_expand_out_clicked() {
    QPushButton *button_out = (QPushButton *)sender();
    int out_number = buttons_outer_to_num[button_out];
    int port_number = button_outer_to_port_num[button_out];
    QTableWidget *table_out = nullptr;
    QTableWidget *table = ui->table_stat;
    int out_row = -1;
    for (int i = 1; i < table->rowCount(); i += 2) {
        QLabel *label0 = dynamic_cast<QLabel *>(table->cellWidget(i - 1, 2));
        if (label0->text().toInt() == port_number) {
            QWidget *tmp0 = table->cellWidget(i, 1);
            QVBoxLayout *layout = dynamic_cast<QVBoxLayout *>(tmp0->layout());
            QTableWidget *tmp = dynamic_cast<QTableWidget *>(layout->itemAt(0)->widget());
            for (int j = 0; j < tmp->rowCount(); j += 2) {
                QLabel *label = dynamic_cast<QLabel *>(tmp->cellWidget(j, 2));
                if (label->text().toInt() == out_number) {
                    table_out = tmp;
                    out_row = j;
                    break;
                }
            }
            if (table_out != nullptr) break;
        }
    }
    GUIStat &port_stat = port_to_gui[port_number];
    GUIStat &out_stat = port_stat.childs[out_number];
    out_row++;
    if (out_stat.expand->text() == "+") {
        table_out->showRow(out_row);
        out_stat.expand->setText("-");
        table_out->setFixedHeight(table_out->verticalHeader()->length() + 20);
        table->resizeRowsToContents();
    } else {
        table_out->hideRow(out_row);
        out_stat.expand->setText("+");
        table_out->setFixedHeight(table_out->verticalHeader()->length() + 20);
        table->resizeRowsToContents();
    }
}

void WindowLGMISPlayer::make_stat_table() {
    QTableWidget *table = ui->table_stat;
    while (table->rowCount() > 0)
    {
        table->removeRow(0);
    }
    port_to_gui.clear();
    buttons_port_to_num.clear();
    buttons_outer_to_num.clear();
    button_outer_to_port_num.clear();

    for (auto it = stats.begin(); it != stats.end(); ++it) {
        StatPort &port = *it;
        int port_row = table->rowCount();
        table->setRowCount(table->rowCount() + 1);

        QPushButton *button_expand_port = new QPushButton("+", table);
        buttons_port_to_num.insert(button_expand_port, port.GetNumber());
        connect(button_expand_port, SIGNAL(clicked()), this, SLOT(on_button_expand_port_clicked()));
        table->setCellWidget(port_row, 0, button_expand_port);

        QLabel *label_port = new QLabel("Порт ", table);
        table->setCellWidget(port_row, 1, label_port);

        QLabel *label_port_num = new QLabel(" " + QString::number(port.GetNumber()), table);
        table->setCellWidget(port_row, 2, label_port_num);

        QCheckBox *check_port = new QCheckBox(table);
        check_port->setChecked(true);
        table->setCellWidget(port_row, 3, check_port);

        QPlainTextEdit *port_percents = new QPlainTextEdit(distances_to_string(make_distances(port.GetPercents())), table);
        port_percents->setReadOnly(true);
        {
            QFontMetrics m(port_percents->font());
            int row_height = m.lineSpacing();
            port_percents->setFixedHeight(3 * row_height);
        }
        table->setCellWidget(port_row, 4, port_percents);
        GUIStat stat_port;
        stat_port.check = check_port;
        stat_port.expand = button_expand_port;
        stat_port.number = port.GetNumber();
        stat_port.type = GUIStat::GUI_PORT;

        //make outer channels
        QWidget *widget_out = new QWidget(table);
        QVBoxLayout *out_layout = new QVBoxLayout(table);
        QTableWidget *table_out = new QTableWidget(0, 5, table);
        out_layout->addWidget(table_out);
        widget_out->setLayout(out_layout);
        table_out->horizontalHeader()->hide();
        table_out->verticalHeader()->hide();
        table_out->horizontalHeader()->setStretchLastSection(true);
        table_out->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
        table_out->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
        QMap<int, StatOuterChan> &outers = port.GetOuterChans();
        for (auto it2 = outers.begin(); it2 != outers.end(); ++it2) {
            StatOuterChan &outer_chan = *it2;
            int out_row = table_out->rowCount();
            table_out->setRowCount(table_out->rowCount() + 1);

            QPushButton *button_expand_out = new QPushButton("+", table_out);
            buttons_outer_to_num.insert(button_expand_out, outer_chan.GetNumber());
            button_outer_to_port_num.insert(button_expand_out, port.GetNumber());
            connect(button_expand_out, SIGNAL(clicked()), this, SLOT(on_button_expand_out_clicked()));
            table_out->setCellWidget(out_row, 0, button_expand_out);

            QLabel *label_out = new QLabel("Внешний канал ", table_out);
            table_out->setCellWidget(out_row, 1, label_out);

            QLabel *label_out_num = new QLabel(" " + QString::number(outer_chan.GetNumber()), table_out);
            table_out->setCellWidget(out_row, 2, label_out_num);

            QCheckBox *check_out = new QCheckBox(table_out);
            check_out->setChecked(true);
            table_out->setCellWidget(out_row, 3, check_out);

            QPlainTextEdit *out_percents = new QPlainTextEdit(distances_to_string(make_distances(outer_chan.GetPercents())), table_out);
            out_percents->setReadOnly(true);
            {
                QFontMetrics m(out_percents->font());
                int row_height = m.lineSpacing();
                out_percents->setFixedHeight(3 * row_height);
            }
            table_out->setCellWidget(out_row, 4, out_percents);
            GUIStat stat_out;
            stat_out.check = check_out;
            stat_out.expand = button_expand_out;
            stat_out.number = outer_chan.GetNumber();
            stat_out.type = GUIStat::GUI_OUTER;

            //make inner channels
            QWidget *widget_in = new QWidget(table_out);
            QVBoxLayout *layout_in = new QVBoxLayout(table_out);
            QTableWidget *table_in = new QTableWidget(0, 3, table_out);
            layout_in->addWidget(table_in);
            widget_in->setLayout(layout_in);
            table_in->horizontalHeader()->hide();
            table_in->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
            table_in->verticalHeader()->hide();
            table_in->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
            QMap<int, StatInnerChan> &inners = outer_chan.GetInnerChans();
            for (auto it3 = inners.begin(); it3 != inners.end(); ++it3) {
                StatInnerChan &inner_chan = *it3;
                int in_row = table_in->rowCount();
                table_in->setRowCount(table_in->rowCount() + 1);

                QLabel *label_in = new QLabel("Внутренний канал ", table_in);
                table_in->setCellWidget(in_row, 0, label_in);

                QLabel *label_in_num = new QLabel(" " + QString::number(inner_chan.GetNumber()), table_in);
                table_in->setCellWidget(in_row, 1, label_in_num);

                QPlainTextEdit *in_percents = new QPlainTextEdit(distances_to_string(make_distances(inner_chan.GetPercents())), table_in);
                in_percents->setReadOnly(true);
                {
                    QFontMetrics m(in_percents->font());
                    int row_height = m.lineSpacing();
                    in_percents->setFixedHeight(3 * row_height);
                }
                table_in->setCellWidget(in_row, 2, in_percents);
                GUIStat stat_in;
                stat_in.check = nullptr;
                stat_in.expand = nullptr;
                stat_in.number = inner_chan.GetNumber();
                stat_in.type = GUIStat::GUI_INNER;
                stat_out.childs.insert(inner_chan.GetNumber(), stat_in);
            }
            table_in->horizontalHeader()->setStretchLastSection(true);
            table_in->setFixedHeight(table_in->verticalHeader()->length() + 20);
            int in_row = table_out->rowCount();
            table_out->setRowCount(table_out->rowCount() + 1);
            table_out->setCellWidget(in_row, 1, widget_in);
            table_out->setSpan(in_row, 1, 1, 4);
            table_out->resizeRowsToContents();
            table_out->hideRow(in_row);
            stat_port.childs.insert(outer_chan.GetNumber(), stat_out);
        }
        int out_row = table->rowCount();
        table->setRowCount(table->rowCount() + 1);
        table_out->setFixedHeight(table_out->verticalHeader()->length() + 20);
        table->setCellWidget(out_row, 1, widget_out);
        table->setSpan(out_row, 1, 1, 4);
        table->resizeRowsToContents();
        table->hideRow(out_row);
        port_to_gui.insert(port.GetNumber(), stat_port);
    }
}

void WindowLGMISPlayer::worker_finished() {
    static const char *__func_name = "WindowLGMISPlayer::worker_finished";
    this->on_button_stop_clicked();
    Logger::Log(LT_DEBUG) << __func_name << "(): worker is finished\n";
    pcap_close(fp);
    state = WindowLGMISPlayer::STOPED;
    worker->deleteLater();
    this->ui->button_play->setEnabled(true);
    this->ui->button_stop->setEnabled(true);
    this->ui->button_make_analyze->setEnabled(true);
    this->ui->button_choose_analyze->setEnabled(true);
    this->ui->button_read_analyze->setEnabled(true);
    in_stat_making = false;
    //save in file
    QFile file(ui->line_dump_path->text() + "_analyzed.txt");
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);
    out << stats;
    file.close();
    Logger::TurnOnLogType(LT_DEBUG);
    Logger::Log(LT_DEBUG) << __func_name << "(): stats serialized\n";
    Logger::TurnOffLogType(LT_DEBUG);
    make_stat_table();
}

void WindowLGMISPlayer::on_button_play_clicked()
{
    static const char *__func_name = "WindowLGMISPlayer::on_button_play_clicked";

    if (state == WindowLGMISPlayer::PLAYING) {
        LogFFL(LT_DEBUG) << "in pausing\n";
        //make pause
        worker->waits.at("pause").make_waiting();
        this->ui->button_play->setText("Continue");
        state = WindowLGMISPlayer::PAUSED;
        return;
    }

    if (state == WindowLGMISPlayer::STOPED) {
        PCAPThread *tmp = new PCAPThread(this);
        tmp->socket = nullptr;
        connect(tmp, SIGNAL(progress_changed(int)), this, SLOT(progress_changed(int)));
        connect(this, SIGNAL(finish_worker()), tmp, SLOT(quit()));
        connect(tmp, SIGNAL(finished()), this, SLOT(worker_finished()));
        worker = tmp;

        std::string src = this->ui->line_dump_path->text().toStdString();
        if (src.length() == 0) {
            Logger::Log(LT_DEBUG) << __func_name << "(): file not selected\n";
            return;
        }

        if (pcap_createsrcstr(source, PCAP_SRC_FILE, NULL, NULL, src.c_str(), errbuf)) {
            Logger::Log(LT_DEBUG) << __func_name << "(): Error with creating a source string\n";
            return;
        }
        this->open_fp();
        if (!fp) {
            Logger::Log(LT_DEBUG) << __func_name << "(): Unable to open the file " << source << "\n";
            return;
        }
        get_packets_count();
        pcap_close(fp);
        this->open_fp();
        if (!fp) {
            Logger::Log(LT_DEBUG) << __func_name << "(): Unable to reopen the file " << source << "\n";
            return;
        }

        bool ok = true;
        if (!in_stat_making) {
            if (this->ui->line_ipaddr->text() == "localhost") {
                worker->addr = QHostAddress::LocalHost;
            } else worker->addr = QHostAddress(this->ui->line_ipaddr->text());
            if (!ok) {
                Logger::Log(LT_DEBUG) << __func_name << "(): port is incorrect\n";
                pcap_close(fp);
                return;
            }
            this->speed_coef = this->ui->line_speed->text().toFloat(&ok);
            if (!ok) {
                Logger::Log(LT_DEBUG) << __func_name << "(): speed coefficient is incorrect\n";
                pcap_close(fp);
                return;
            }
        }
        packets_iterator = 0;
        worker->player = this;
        worker->terminate = false;
        state = WindowLGMISPlayer::PLAYING;
        if (!in_stat_making) {
            this->ui->button_stop->setEnabled(true);
            this->ui->line_ipaddr->setEnabled(false);
            this->ui->pushButton->setEnabled(false);
            this->ui->line_speed->setEnabled(false);
            this->ui->button_play->setText("Pause");
            this->ui->button_goto->setEnabled(true);
            this->ui->button_make_analyze->setEnabled(false);
            this->ui->button_read_analyze->setEnabled(false);
        }
        worker->start();
        return;
    }

    if (state == WindowLGMISPlayer::PAUSED) {
        //make play from iterator
        Logger::Log(LT_DEBUG) << __func_name << "(): before continuing\n";
        worker->waits.at("pause").stop_waiting();
        Logger::Log(LT_DEBUG) << __func_name << "(): after waiting\n";
        this->ui->button_play->setText("Pause");
        state = WindowLGMISPlayer::PLAYING;
        return;
    }
}

void WindowLGMISPlayer::waste_to(int number)
{
    pcap_close(fp);
    fp = pcap_open(source, 65536, PCAP_OPENFLAG_PROMISCUOUS, 1000, NULL, errbuf);
    int pack_num = (packets_count + 0.0) / 100 * number;
    struct pcap_pkthdr *header;
    const u_char *pkt_data;
    for (int i = 0; i < pack_num; ++i) {
        pcap_next_ex(fp, &header, &pkt_data);
    }
    prev_packet = header->ts;
    packets_iterator = pack_num;
}

WindowLGMISPlayer::~WindowLGMISPlayer()
{
    delete ui;
}

void WindowLGMISPlayer::get_packets_count() {
    static const char *__func_name = "WindowLGMISPlayer::get_packets_count";
    if (state == WindowLGMISPlayer::STOPED) {
        packets_count = 0;
        struct pcap_pkthdr *header;
        const u_char *pkt_data;
        while (pcap_next_ex(fp, &header, &pkt_data) > 0) ++packets_count;
    }
    Logger::Log(LT_DEBUG) << __func_name << "(): packets_count: " << packets_count << "\n";
}

void WindowLGMISPlayer::progress_changed(int percent) {
    this->ui->progress_bar->setValue(percent);
}

void WindowLGMISPlayer::on_pushButton_clicked()
{
    static const char *__func_name = "WindowLGMISPlayer::on_pushButton_clicked";
    QString filename = QFileDialog::getOpenFileName(this, tr("Select dump file (pcap)"), "./", tr("Dump files"));
    if (filename.length() < 1) {
        Logger::Log(LT_INFO) << __func_name << "(): no file selected\n";
        return;
    }
    this->ui->line_dump_path->setText(filename);
}

void WindowLGMISPlayer::on_button_stop_clicked()
{
    static const char *__func_name = "WindowLGMISPlayer::on_button_stop_clicked";
    worker->terminate = true;
    if (state == WindowLGMISPlayer::PAUSED) {
        this->on_button_play_clicked();
    }
    Logger::Log(LT_DEBUG) << __func_name << "(): terminate signal sended\n";
    emit this->finish_worker();
    this->ui->line_ipaddr->setEnabled(true);
    this->ui->pushButton->setEnabled(true);
    this->ui->button_play->setText("Play");
    this->ui->button_play->setEnabled(false);
    this->ui->button_stop->setEnabled(false);
    this->ui->line_speed->setEnabled(true);
    this->ui->button_goto->setEnabled(false);
    this->ui->progress_bar->setValue(0);
    this->ui->button_make_analyze->setEnabled(false);
    this->ui->button_read_analyze->setEnabled(false);
}

void WindowLGMISPlayer::on_button_goto_clicked()
{
    static const char *__func_name = "WindowLGMISPlayer::on_button_goto_clicked";
    bool ok;
    int val = this->ui->line_goto->text().toInt(&ok);
    if (!ok || (val > 100) || (val < 0)) {
        Logger::Log(LT_DEBUG) << __func_name << "(): error in goto\n";
        return;
    }
    bool block = true;
    if (state == WindowLGMISPlayer::PAUSED) block = false;
    worker->waits.at("waste").make_waiting(block);
    Logger::Log(LT_DEBUG) << __func_name << "(): worker is waiting\n";
    waste_to(val);
    Logger::Log(LT_DEBUG) << __func_name << "(): packet_iterator = " << packets_iterator << "\n";
    this->ui->progress_bar->setValue(val);
    Logger::Log(LT_DEBUG) << __func_name << "(): worker can continue\n";
    worker->wasted = true;
    worker->waits.at("waste").stop_waiting();
}

void WindowLGMISPlayer::on_button_make_analyze_clicked()
{
    this->ui->button_choose_analyze->setEnabled(false);
    this->ui->pushButton->setEnabled(false);
    this->ui->button_play->setEnabled(false);
    in_stat_making = true;
    this->ui->button_make_analyze->setEnabled(false);
    this->ui->button_read_analyze->setEnabled(false);
    this->on_button_play_clicked();
}

void WindowLGMISPlayer::on_button_choose_analyze_clicked()
{
    static const char *__func_name = "WindowLGMISPlayer::on_button_choose_analyze_clicked";
    QString filename = QFileDialog::getOpenFileName(this, tr("Select analyze file"), "./", tr("Analyze files"));
    if (filename.length() < 1) {
        Logger::Log(LT_INFO) << __func_name << "(): no file selected\n";
        return;
    }
    this->ui->line_analyze_path->setText(filename);
}

void WindowLGMISPlayer::on_button_read_analyze_clicked()
{
    static const char *__func_name = "WindowLGMISPlayer::on_button_read_analyze_clicked";
    if (!ui->line_analyze_path->text().length()) {
        Logger::Log(LT_DEBUG) << __func_name << "(): file name not specified\n";
        return;
    }
    QFile file(ui->line_analyze_path->text());
    file.open(QIODevice::ReadOnly);
    if (!file.isOpen()) {
        Logger::Log(LT_DEBUG) << __func_name << "(): error with opening file\n";
        return;
    }
    QDataStream in(&file);
    in >> stats;
    Logger::Log(LT_INFO) << __func_name << "(): stats read from file: \n";
    for (auto it = stats.begin(); it != stats.end(); ++it) {
        Logger::Log(LT_INFO) << __func_name << "(): stats: " << *it << "\n";
    }
    file.close();
    make_stat_table();
}
