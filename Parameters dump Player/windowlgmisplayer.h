#ifndef WINDOWLGMISPLAYER_H
#define WINDOWLGMISPLAYER_H

#include <QMainWindow>
#include <QMutex>
#include <QMutexLocker>
#include <QtConcurrent/QtConcurrentRun>
#include <QUdpSocket>
#include <map>
#include <memory>
#include "custom_pcap.h"
#include "statclasses.h"
#include <QPushButton>
#include <QCheckBox>

namespace Ui {
class WindowLGMISPlayer;
}

class WindowLGMISPlayer;

//~~~~~~~~~~~~~~~~~~~~~~~~ W A I T  F O R ~~~~~~~~~~~~~~~~~~~~~~~~

struct wait_for {
    bool wait_please;
    bool continue_please;
    bool is_waiting;
    wait_for();
    bool start_wait();
    bool make_waiting(bool block = true);
    bool stop_waiting();
private:
    std::shared_ptr<QMutex> mt;
};

//~~~~~~~~~~~~~~~~~~~~~~~~ P C A P   T H R E A D ~~~~~~~~~~~~~~~~~~~~~~~~

class PCAPThread : public QThread {
    Q_OBJECT

signals:
    void progress_changed(int percent);

public slots:

public:
    PCAPThread(WindowLGMISPlayer *_player);

private:
    void run();

    WindowLGMISPlayer *player;
    QUdpSocket *socket;
    QHostAddress addr;

    bool terminate;
    bool wasted;

    std::map<std::string, wait_for> waits;

    friend class WindowLGMISPlayer;
};

//~~~~~~~~~~~~~~~~~~~~~~~~ G U I   S T A T ~~~~~~~~~~~~~~~~~~~~~~~~

struct GUIStat {
    enum GUIStatType {
        GUI_PORT = 0, GUI_INNER = 1, GUI_OUTER = 2, GUI_UNDEF = 3
    };

    int number;
    QPushButton *expand;
    QCheckBox *check;
    GUIStatType type;
    QMap<int, GUIStat> childs;
    GUIStat();
    GUIStat(int number_, QPushButton *expand_, QCheckBox *check_, GUIStatType type_);
};

std::ostream &operator<<(std::ostream &stream, const GUIStat &ob);

//~~~~~~~~~~~~~~~~~~~~~~~~ W I N D O W   L G M I S   P L A Y E R ~~~~~~~~~~~~~~~~~~~~~~~~

class WindowLGMISPlayer : public QMainWindow
{
    Q_OBJECT

public:
    enum PlayerState {
        PLAYING, STOPED, PAUSED
    };

    explicit WindowLGMISPlayer(QWidget *parent = 0);
    ~WindowLGMISPlayer();

signals:
    void finish_worker();

private slots:
    void on_button_play_clicked();

    void on_pushButton_clicked();

    void on_button_stop_clicked();

    void progress_changed(int percent);

    void on_button_goto_clicked();

    void worker_finished();

    void on_button_make_analyze_clicked();

    void on_button_choose_analyze_clicked();

    void on_button_read_analyze_clicked();

    void on_button_expand_port_clicked();

    void on_button_expand_out_clicked();

private:
    Ui::WindowLGMISPlayer *ui;

    int packets_count;
    int packets_iterator;
    struct timeval prev_packet;
    float speed_coef;

    QMap<int, GUIStat> port_to_gui;

    QMap<QPushButton *, int> buttons_port_to_num;
    QMap<QPushButton *, int> buttons_outer_to_num;
    QMap<QPushButton *, int> button_outer_to_port_num;

    QMap<int, StatPort> stats;
    bool in_stat_making;

    PlayerState state;

    PCAPThread *worker;
    pcap_t *fp;
    char errbuf[PCAP_ERRBUF_SIZE];
    char source[PCAP_BUF_SIZE];

    void get_packets_count();
    void waste_to(int number);
    void open_fp();
    void make_stat_table();

    friend class PCAPThread;
};

#endif // WINDOWLGMISPLAYER_H
