#-------------------------------------------------
#
# Project created by QtCreator 2015-11-20T14:50:25
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ParametersDumpPlayer
TEMPLATE = app

CONFIG += c++11

INCLUDEPATH += ../bin/external/pcaplib/Include
INCLUDEPATH += ../logger/ ../MValue/

CONFIG(debug, debug|release) {
    TARGET = ParametersDumpPlayerd
    DESTDIR = ../../bin/debug
    LIBS += -L../../bin/debug/ -lloggerd
    LIBS += -L../../bin/debug/ -lmvalued
} else {
    TARGET = ParametersDumpPlayer
    DESTDIR = ../../bin/release
    LIBS += -L../../bin/release/ -llogger
    LIBS += -L../../bin/release/ -lmvalue
}

LIBS += -L../bin/external/pcaplib/Lib/ -lwpcap -lPacket
LIBS += -lWs2_32

SOURCES += main.cpp\
        windowlgmisplayer.cpp \
    networkpackets.cpp \
    statclasses.cpp

HEADERS  += windowlgmisplayer.h \
    WpdPack/Include/pcap.h \
    custom_pcap.h \
    networkpackets.h \
    statclasses.h

FORMS    += windowlgmisplayer.ui

OTHER_FILES += \
    pause.png \
    stop.png \
    Play.ico
