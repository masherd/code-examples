import struct

from tornado.gen import coroutine

class NetworkType:
    test = 0
    success = 1
    undefined = 2
    error = 3
    #~~~~~~~~ M o d e   t y p e s ~~~~~~~~
    data = 4
    register_request = 5
    unregister_request = 6
    all_slots_occupied = 7
    drop_to_log = 8
    add_params = 9
    added_params = 10
    remove_params = 11
    request_params = 12
    params_values = 13
    #~~~~~~~~ D a t a   t y p e s ~~~~~~~~
    int8__t = 0
    uint8__t = 1
    int16__t = 2
    uint16__t = 3
    int32__t = 4
    uint32__t = 5
    int64__t = 6
    uint64__t = 7
    string__t = 8
    double__t = 9
    bool__t = 10
    array__t = 11
    map__t = 12
    binary__t = 13
    nil__t = 14
    tmi_config__t = 15
    param_info__t = 16
    param_val__t = 17

class SkipTypes:
    SkipAll = 0
    SkipOne = 1
    NoSkip = 2

    @staticmethod
    def must_skip(tp):
        return tp == SkipTypes.SkipAll or tp == SkipTypes.SkipOne

MIN_INT64 = -(1 << 63) + 1
MIN_INT32 = -(1 << 31) + 1
MIN_INT16 = -(1 << 15) + 1
MIN_INT8 = -(1 << 7) + 1

SLOT_2_0   = 0x001fc07f
SLOT_4_2_0 = 0xf01fc07f

def minimize_number_of_bits(number):
    if number >= 0:
        return number
    if number >= MIN_INT8:
        buf = bytearray((struct.pack('<b', number)))
        res = struct.unpack('<B', buf)[0]
        return res

def signed_int_to_unsigned(number, type_sz):
    mode = 'b'
    if type_sz == 2:
        mode = 'h'
    elif type_sz == 4:
        mode = 'i'
    elif type_sz == 8:
        mode = 'q'
    data = struct.pack('<'+mode, number)
    result = struct.unpack('<'+mode.upper(), data)[0]
    return result

def unsigned_int_to_signed(number, type_sz):
    mode = 'B'
    if type_sz == 2:
        mode = 'H'
    elif type_sz == 4:
        mode = 'I'
    elif type_sz == 8:
        mode = 'Q'
    data = struct.pack('<'+mode, number)
    result = struct.unpack('<'+mode.lower(), data)[0]
    return result

class Varint(object):
    @staticmethod
    def varint_len(number):
        if number >> 63:
            return 9
        i = 1
        while True:
            number = number >> 7
            if number == 0:
                break;
            i += 1
        return i

    @staticmethod
    def _encode_varint64(number):
        buf = bytearray()
        res = bytearray()
        buf += bytes([0]) * 10
        if number & (0xff000000<<32):
            res += bytes([0]) * 9
            res[8] = number & 0xff
            number >>= 8
            i = 7
            while i >= 0:
                res[i] = ((number & 0x7f) | 0x80) & 0xff
                number >>= 7
                i -= 1
            return (9, res)
        n = 0
        while True:
            buf[n] = 0xff & ((number & 0x7f) | 0x80)
            n += 1
            number >>= 7
            if number == 0:
                break
        buf[0] &= 0x7f
        i = 0
        j = n - 1
        res += bytes([0]) * n
        while True:
            if j < 0:
                break
            res[i] = buf[j]
            j -= 1
            i += 1
        return (n, res)

    @staticmethod
    def encode(number):
        res = bytearray()
        if number < 0x7f:
            res = res + bytes([0])
            res[0] = number & 0x7f
            return (1, res)
        if number < 0x3fff:
            res += bytes([0]) * 2
            res[0] = ((number >> 7) & 0x7f) | 0x80
            res[1] = number & 0x7f
            return (2, res)
        return Varint._encode_varint64(number)

    @staticmethod
    def decode(data):
        offset = 0
        a = int(data[offset])
        if (a & 0x80) == 0:
            return (1, a)
        offset += 1

        b = int(data[offset])
        if (b & 0x80) == 0:
            a &= 0x7f
            a <<= 7
            a |= b
            return (2, a)
        offset += 1

        a <<= 14
        a |= int(data[offset])
        if (a & 0x80) == 0:
            a &= SLOT_2_0
            b &= 0x7f
            b <<= 7
            a |= b
            return (3, a)

        a &= SLOT_2_0
        offset += 1
        b <<= 14
        b |= int(data[offset])
        if (b & 0x80) == 0:
            b &= SLOT_2_0
            a <<= 7
            a |= b
            return (4, a)
        b &= SLOT_2_0
        s = a
        offset += 1
        a <<= 14
        a |= int(data[offset])
        if (a & 0x80) == 0:
            b <<= 7
            a |= b
            s >>= 18
            return (5, (s << 32) | a)

        s <<= 7
        s |= b
        offset += 1
        b <<= 14
        b |= int(data[offset])
        if (b & 0x80) == 0:
            a &= SLOT_2_0
            a <<= 7
            a |= b
            s >>= 18
            return (6, (s << 32) | a)

        offset += 1
        a <<= 14
        a |= int(data[offset])
        if (a & 0x80) == 0:
            a &= SLOT_4_2_0
            b &= SLOT_2_0
            b <<= 7
            a |= b
            s >>= 11
            return (7, (s << 32) | a)

        a &= SLOT_2_0
        offset += 1
        b <<= 14
        b |= int(data[offset])
        if (b & 0x80) == 0:
            b &= SLOT_4_2_0
            a <<= 7
            a |= b
            s >>= 4
            return (8, (s << 32) | a)

        offset += 1
        a <<= 15
        a |= int(data[offset])
        b &= SLOT_2_0
        b <<= 8
        a |= b
        s <<= 4
        b = int(data[offset - 4])
        b &= 0x7f
        s |= b
        return (9, (s << 32) | a)

def encode_snetwork_header(mode, size):
    len_of_mode = Varint.varint_len(mode)
    len_of_size = Varint.varint_len(size)
    header = (len_of_mode << 3) | len_of_size
    res = bytearray(bytes([0]))
    res[0] = header
    res += Varint.encode(mode)[1] + Varint.encode(size)[1]
    return res

def decode_snetwork_header(src):
    offset = 1
    length, mode = Varint.decode(src[offset:])
    offset += length
    length, size = Varint.decode(src[offset:])
    offset += length
    return (offset, mode, size)

def get_len_of_header(header):
    return (header & 7) + (header >> 3) & 7


class DataStructure(object):
    def __init__(self, mode=NetworkType.data, data=b''):
        self.mode = mode
        self.data = bytearray(data)
    def settypeofdata(self, t):
        self.data[0] = t
    def gettypeofdata(self):
        return self.data[0]
    def prepare(self):
        return encode_snetwork_header(self.mode, len(self.data)) + bytearray(self.data)
    def __str__(self):
        return 'mode: {}, data_len: {}'.format(self.mode, len(self.data))
    def __repr__(self):
        return self.__str__()

class NetworkUtils(object):
    @staticmethod
    def EncodeInt32(val):
        uval = signed_int_to_unsigned(val, 4)
        data = bytearray(bytes([0]))
        data[0] = NetworkType.int32__t
        data += Varint.encode(uval)[1]
        return DataStructure(data=data)

    @staticmethod
    def EncodeBool(val):
        if val:
            val = 1
        else:
            val = 0
        return DataStructure(data=bytearray(struct.pack('<BB', NetworkType.bool__t, val)))

    @staticmethod
    def DecodeBool(bin, skiptype=SkipTypes.NoSkip):
        if len(bin) < 2:
            print('NetworkUtils::DecodeBool(): len must be 2 but is {}'.format(len(bin)))
            return None
        if not SkipTypes.must_skip(skiptype) and bin[0] != NetworkType.bool__t:
            print('NetworkUtils::DecodeBool(): error with type')
            return None
        if int(bin[1]) == 0: return False
        else: return True

    @staticmethod
    def DecodeInt32(bin, skiptype=SkipTypes.NoSkip):
        if not SkipTypes.must_skip(skiptype) and bin[0] != NetworkType.int32__t:
            print('NetworkUtils::DecodeInt32(): error with type')
            return None
        number = Varint.decode(bin[1:])[1]
        return unsigned_int_to_signed(number, 4)

    @staticmethod
    def DecodeUint32(bin, skiptype=SkipTypes.NoSkip):
        if not SkipTypes.must_skip(skiptype) and bin[0] != NetworkType.uint32__t:
            print('NetworkUtils::DecodeUint32(): error with type')
            return None
        return Varint.decode(bin[1:])[1]

    @staticmethod
    def DecodeInt64(bin, skiptype=SkipTypes.NoSkip):
        if not SkipTypes.must_skip(skiptype) and bin[0] != NetworkType.int64__t:
            print('NetworkUtils::DecodeInt64(): error with type')
            return None
        number = Varint.decode(bin[1:])[1]
        return unsigned_int_to_signed(number, 8)

    @staticmethod
    def DecodeDouble(bin, skiptype=SkipTypes.NoSkip):
        if len(bin) < 9:
            print('NetworkUtils::DecodeDouble(): len must be 9 but is {}'.format(len(bin)))
            return None
        if not SkipTypes.must_skip(skiptype) and bin[0] != NetworkType.double__t:
            print('NetworkUtils::DecodeDouble(): error with type')
            return None
        return struct.unpack('<d', bin[1:9])[0]

    @staticmethod
    def ReadInt32(bin):
        return struct.unpack('<I', bin[:4])[0]

    @staticmethod
    def EncodeUint32(val):
        uval = signed_int_to_unsigned(val, 4)
        data = bytearray(bytes([0]))
        data[0] = NetworkType.uint32__t
        data += Varint.encode(uval)[1]
        return DataStructure(data=data)

    @staticmethod
    def EncodeStr(val):
        return DataStructure(data=(bytearray(struct.pack('<B', NetworkType.string__t))+bytearray(val.encode('utf_16')[2:])))

    @staticmethod
    def DecodeStr(bin, skiptype=SkipTypes.NoSkip):
        if not SkipTypes.must_skip(skiptype) and bin[0] != NetworkType.string__t:
            print('NetworkUtils::DecodeStr(): error with type')
            return None
        return bin[1:].decode('utf_16')

    @staticmethod
    def EncodeBin(val):
        return DataStructure(data=bytearray(struct.pack('<B', NetworkType.binary__t))+bytearray(val))

    @staticmethod
    def create_array():
        return DataStructure(data=bytearray(struct.pack('<B', NetworkType.array__t)))

    @staticmethod
    def split_to_array(data, skiptype=SkipTypes.NoSkip):
        if not SkipTypes.must_skip(skiptype) and data[0] != NetworkType.array__t:
            print('Incorrect key array')
            return []
        was_read = 1
        size = len(data)
        res = []
        data = bytes(data)
        while was_read < size:
            length, mode, sz = decode_snetwork_header(data[was_read:])
            was_read += length
            dt = data[was_read:was_read+sz]
            was_read += sz
            res.append(DataStructure(mode=mode, data=dt))
        return res

    @staticmethod
    def append_to_array(src, data):
        src.data = bytearray(src.data) + encode_snetwork_header(data.mode, len(data.data)) + bytearray(data.data)

    @staticmethod
    @coroutine
    def read_data_structure(stream):
        buf = yield stream.read_bytes(1)
        header = bytearray(buf)
        len_of_header = get_len_of_header(int(header[0]))
        buf = yield stream.read_bytes(len_of_header)
        header += bytes(buf)
        length, mode, size = decode_snetwork_header(header)
        data = b''
        if size:
            data = yield stream.read_bytes(size)
        return DataStructure(data=data, mode=mode)
