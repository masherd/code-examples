var global_client = null;

function show_answer_callback(answer) {
	console.log(answer);
}

function register_client() {
	global_client = new TMIClient();
	config = new TMIConfig();
	config.tmi_type = 0;
	config.db_name = "db_db.db";
	config.port = 12345;
	config.outer_channel = 987;
	config.inner_channel = 789;
	global_client.connect(config, show_answer_callback);
}

function test_add_parameters() {
	params = [];
	params.push(new ParameterInfo("param1", true));
	params.push(new ParameterInfo("param2", true));
	params.push(new ParameterInfo("param3", false));
	params.push(new ParameterInfo("параметр4", false));
	global_client.add_parameters(params, show_answer_callback);
}

function test_remove_parameters() {
	params = [];
	params.push(new ParameterInfo("param2", true));
	params.push(new ParameterInfo("параметр4", false));
	global_client.remove_parameters(params, show_answer_callback);
}

function test_request_parameters() {
	params = [];
	params.push(new ParameterInfo("param1", true));
	params.push(new ParameterInfo("param2", true));
	params.push(new ParameterInfo("param3", false));
	params.push(new ParameterInfo("параметр4", false));
	global_client.request_parameters(params, show_answer_callback);
}
