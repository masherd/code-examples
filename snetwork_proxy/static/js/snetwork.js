var RequestModes = {
	test: 0, success: 1, undef: 2, error: 3,

    //~~~~~~~~ M o d e   t y p e s ~~~~~~~~
    data: 4, register_request: 5, unregister_request: 6,
	all_slots_occupied: 7, drop_to_log: 8, last_reserved_mode_type: 8,
	add_params: 9, added_params: 10, remove_params: 11,
    request_params: 12, params_values: 13,

    //~~~~~~~~ D a t a   t y p e s ~~~~~~~~
    int8__t: 0, uint8__t: 1, int16__t: 2, uint16__t: 3,
    int32__t: 4, uint32__t: 5, int64__t: 6, uint64__t: 7,
    string__t: 8, double__t: 9, bool__t: 10, array__t: 11,
    map__t: 12, binary__t: 13, nil__t: 14, tmi_config__t: 15,
    param_info__t: 16, param_val__t: 17
};

var ValueTypes = {
	STRING_TYPE: 0, DOUBLE_TYPE: 1, INT_TYPE: 2,
	UINT_TYPE: 3, BOOL_TYPE: 4, NONE_TYPE: 5
}

function TMIConfig() {
    this.tmi_type = 0;
    this.db_name = '';
    this.port = -1;
    this.outer_channel = -1;
    this.inner_channel = -1;
    this.toJSON = function() {
        return {tmi_type: this.tmi_type, db_name: this.db_name,
            port: this.port, outer_channel: this.outer_channel,
            inner_channel: this.inner_channel};
    };
}

function ParameterInfo(name, is_digital) {
    this.name = name;
    this.is_digital = is_digital;
    this.toJSON = function() {
        return {name: this.name, is_digital: this.is_digital};
    };
    this.fromDict = function(dict) {
        this.name = dict['name'];
        this.is_digital = Boolean(dict['is_digital']);
        return true;
    };
}

function ParameterValue() {
    this.value = null;
    this.secs = 0;
    this.msecs = 0;
    this.info = new ParameterInfo('', true);
    this.val_type = ValueTypes.NONE_TYPE;
    this.fromDict = function(dict) {
        this.value = dict['value'];
        this.secs = dict['secs'];
        this.msecs = dict['msecs'];
        if (!this.info.fromDict(dict['info'])) {
            return false;
        }
        this.val_type = dict['val_type'];
        return true;
    };
}

function getXmlHttp(){
    var xmlhttp;
    try {
        xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
        try {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        } catch (E) {
            xmlhttp = false;
        }
    }
    if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
        xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
}

function TMIClient() {
    this.is_registered = false;
    this.send_form_data = function(form_data, prepare_func, callback) {
        var local_server = getXmlHttp();
        local_server.onreadystatechange = function() {
            if (local_server.readyState == 4) {
                res = local_server.responseText;
                if (prepare_func) {
                    res = prepare_func(res);
                } else {
                    res = JSON.parse(local_server.responseText);
                }
                if (callback) {
                    callback(res);
                }
            }
        };
        local_server.open('POST', '/get_data');
        local_server.send(form_data);
        console.log('Data is send');
    }
    this.send_param_infos = function(params, mode, prepare_func, callback) {
        var fd = new FormData();
        fd.append(mode, JSON.stringify(params));
        this.send_form_data(fd, prepare_func, callback);
    }
    this.connect = function(tmi_config, callback) {
        var fd = new FormData();
        client = this;
        fd.append('register_request', JSON.stringify(tmi_config));
        client.send_form_data(fd, function(answer) {
                answer = JSON.parse(answer);
                if ('error' in answer) return answer;
                client.is_registered = true;
                return answer['response'];
            },
            callback);
    };
    this.add_parameters = function(params, callback) {
        this.send_param_infos(params, 'add_params', function(answer) {
                answer = JSON.parse(answer);
                if ('error' in answer) return answer;
                answer = answer['response'];
                res = []
                for (i = 0; i < answer.length; ++i) {
                    tmp = {}
                    tmp['success'] = answer[i][1];
                    tmp['info'] = new ParameterInfo();
                    tmp['info'].fromDict(answer[i][0]);
                    res.push(tmp);
                }
                return res;
            },
            callback);
    };
    this.remove_parameters = function(params, callback) {
        this.send_param_infos(params, 'remove_params', function(answer) {
                answer = JSON.parse(answer);
                if ('error' in answer) return answer;
                return true;
            },
            callback);
    };
    this.request_parameters = function(params, callback) {
        this.send_param_infos(params, 'request_params', function(answer) {
                answer = JSON.parse(answer);
                if ('error' in answer) return answer;
                answer = answer['response'];
                res = []
                for (i = 0; i < answer.length; ++i) {
                    t = new ParameterValue();
                    t.fromDict(answer[i]);
                    res.push(t);
                }
                return res;
            },
            callback);
    };
}
