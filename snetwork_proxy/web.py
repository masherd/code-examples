import argparse
import os
import socket
import json
import struct

from tornado.web import *
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop, PeriodicCallback
from tornado.gen import coroutine
from tornado.tcpclient import TCPClient
from tornado.iostream import StreamClosedError

from network_utils import *

MAX_CLIENT_IDLE = 300 # seconds
CLEAR_CLIENTS_TIME = 10

global_tmi_port = 13000
global_tmi_host = ''
def get_tmiport():
    return global_tmi_port

def get_tmihost():
    return global_tmi_host

global_tcpclient = None
def get_tcpclient():
    global global_tcpclient
    if not global_tcpclient:
        global_tcpclient = TCPClient(io_loop=IOLoop.current())
    return global_tcpclient

global_clients = {}
def get_global_clients():
    global global_clients
    return global_clients

def add_client(id, new_client):
    global global_clients
    id = int(id)
    if id in global_clients:
        return False
    global_clients[id] = new_client
    return True

def get_client(id):
    id = int(id)
    global global_clients
    if id not in global_clients:
        return None
    return global_clients[id]

class TMIConfig(object):
    def __init__(self, assoc):
        self.tmi_type = int(assoc['tmi_type'])
        self.db_name = str(assoc['db_name'])
        self.outer_channel = int(assoc['outer_channel'])
        self.inner_channel = int(assoc['inner_channel'])
        self.port = int(assoc['port'])

    def __str__(self):
        return '[tmi_type: {}, db_name: {}, out_chan: {}, inner_chan: {},'\
            'port: {}]'.format(self.tmi_type, self.db_name, self.outer_channel,
            self.inner_channel, self.port)

    def serialize(self):
        res = NetworkUtils.create_array()
        NetworkUtils.append_to_array(res, NetworkUtils.EncodeInt32(self.tmi_type))
        NetworkUtils.append_to_array(res, NetworkUtils.EncodeStr(self.db_name))
        NetworkUtils.append_to_array(res, NetworkUtils.EncodeInt32(self.port))
        NetworkUtils.append_to_array(res, NetworkUtils.EncodeInt32(self.outer_channel))
        NetworkUtils.append_to_array(res, NetworkUtils.EncodeInt32(self.inner_channel))
        res.settypeofdata(NetworkType.tmi_config__t)
        return res

class RegisterConfig(object):
    def __init__(self, id=-1, password=-1, data=b''):
        self.id = id
        self.password = password
        self.data = bytearray(data)

    def serialize(self):
        res = NetworkUtils.create_array()
        NetworkUtils.append_to_array(res, NetworkUtils.EncodeInt32(self.id))
        NetworkUtils.append_to_array(res, NetworkUtils.EncodeInt32(self.password))
        NetworkUtils.append_to_array(res, NetworkUtils.EncodeBin(self.data))
        return res

    def deserialize(self, data):
        __func_name = "RegisterConfig::deserialize"
        l = NetworkUtils.split_to_array(data)
        if len(l) != 3:
            print('{}(): register config len must be 3, but is {}'.format(__func_name, len(l)))
            return False
        id = NetworkUtils.DecodeInt32(l[0].data)
        if id == None:
            print('{}(): error with converting id'.format(__func_name))
            return False
        password = NetworkUtils.DecodeInt32(l[1].data)
        if password == None:
            print('{}(): error with converting password'.format(__func_name))
            return False
        data = l[2].data
        self.id = id
        self.password = password
        self.data = data
        print('{}(): deserialized'.format(__func_name))
        return True

    def __str__(self):
        return 'id: {}, password: {}, data: [{}]'.format(self.id, self.password, self.data)
    def __repr__(self):
        return self.__str__()

class ParameterInfo(object):
    def __init__(self, name='', is_digital=False):
        self.name = name
        self.is_digital = is_digital

    def __str__(self):
        return 'name: {}, is_digital: {}'.format(self.name, self.is_digital)

    def __repr__(self):
        return self.__str__()

    def tojson(self):
        return {'name': self.name, 'is_digital': self.is_digital}

    def serialize(self):
        res = NetworkUtils.create_array()
        NetworkUtils.append_to_array(res, NetworkUtils.EncodeStr(self.name))
        NetworkUtils.append_to_array(res, NetworkUtils.EncodeBool(self.is_digital))
        res.settypeofdata(NetworkType.param_info__t)
        return res

    def deserialize(self, data):
        __func_name = "ParameterInfo::deserialize"
        l = NetworkUtils.split_to_array(data, SkipTypes.SkipOne)
        if len(l) != 2:
            print('{}(): parameter info len must be 2, but is {}'.format(__func_name, len(l)))
            return False
        name = NetworkUtils.DecodeStr(l[0].data)
        if name == None:
            print('{}(): error with converting name'.format(__func_name))
            return False
        is_digital = NetworkUtils.DecodeBool(l[1].data)
        if is_digital == None:
            print('{}(): error with converting is_digital'.format(__func_name))
            return False
        self.name = name
        self.is_digital = is_digital
        return True

    @staticmethod
    def serialize_list(data):
        req = NetworkUtils.create_array()
        for info in data:
            NetworkUtils.append_to_array(req, info.serialize())
        return req

class ParameterValue(object):
    STRING_TYPE = 0
    DOUBLE_TYPE = 1
    INT_TYPE = 2
    UINT_TYPE = 3
    BOOL_TYPE = 4
    NONE_TYPE = 5

    def __init__(self):
        self.value = None
        self.info = None
        self.val_type = ParameterValue.NONE_TYPE
        self.secs = 0
        self.nsecs = 0

    def __str__(self):
        return 'value: {}, info: [{}], val_type: {}, secs: {}, msecs: {}'\
            .format(self.value, self.info, self.val_type, self.secs, self.msecs)

    def __repr__(self):
        return self.__str__()

    def tojson(self):
        info = None
        if self.info:
            info = self.info.tojson()
        return {'value': self.value, 'info': info, 'val_type': self.val_type,
            'secs': self.secs, 'msecs': self.msecs}

    @staticmethod
    def deserialize_list(data):
        __func_name = "ParameterValue::deserialize_list"
        tmp = NetworkUtils.split_to_array(data)
        if tmp == None:
            print('{}(): error with splitting to list'.format(__func_name))
            return None
        res = []
        for t in tmp:
            pv = ParameterValue()
            if not pv.deserialize(t.data):
                print('{}(): error with deserializing value'.format(__func_name))
                return None
            res.append(pv)
        return res

    def deserialize(self, data):
        __func_name = "ParameterValue::deserialize"
        if data[0] != NetworkType.param_val__t:
            print('{}(): incorrect type'.format(__func_name))
            return False
        lst = NetworkUtils.split_to_array(data, SkipTypes.SkipOne)
        if not lst:
            print('{}(): array must not be empty'.format(__func_name))
            return False
        info = ParameterInfo()
        if not info.deserialize(lst[0].data):
            print('{}(): error with converting info'.format(__func_name))
            return False
        is_set_val = NetworkUtils.DecodeBool(lst[1].data)
        if is_set_val == None:
            print('{}(): error with converting is_set_val'.format(__func_name))
            return False
        val_type = NetworkUtils.DecodeInt32(lst[2].data)
        if val_type == None:
            print('{}(): error with converting val_type'.format(__func_name))
            return False
        if not is_set_val:
            self.info = info
            self.val_type = val_type
            return True
        secs = NetworkUtils.DecodeInt64(lst[3].data)
        if secs == None:
            print('{}(): error with converting secs'.format(__func_name))
            return False
        msecs = NetworkUtils.DecodeInt32(lst[4].data)
        if msecs == None:
            print('{}(): error with converting msecs'.format(__func_name))
            return False
        
        val = None
        if val_type == ParameterValue.STRING_TYPE:
            val = NetworkUtils.DecodeStr(lst[5].data)
            if val == None:
                print('{}(): error with converting str'.format(__func_name))
                return False
            if not info.is_digital:
                print('{}(): error with creating ParameterValue. val is str, '\
                    'but param is not digital'.format(__func_name))
                return False
        elif val_type == ParameterValue.DOUBLE_TYPE:
            val = NetworkUtils.DecodeDouble(lst[5].data)
            if val == None:
                print('{}(): error with converting double'.format(__func_name))
                return False
        elif val_type == ParameterValue.INT_TYPE:
            val = NetworkUtils.DecodeInt32(lst[5].data)
            if val == None:
                print('{}(): error with converting int'.format(__func_name))
                return False
            if not info.is_digital:
                print('{}(): error with creating ParameterValue. val is int, '\
                    'but param is not digital'.format(__func_name))
                return False
        elif val_type == ParameterValue.UINT_TYPE:
            val = NetworkUtils.DecodeUint32(lst[5].data)
            if val == None:
                print('{}(): error with converting uint'.format(__func_name))
                return False
        elif val_type == ParameterValue.BOOL_TYPE:
            val = NetworkUtils.DecodeBool(lst[5].data)
            if val == None:
                print('{}(): error with converting bool'.format(__func_name))
                return False
        else:
            print('{}(): error with creating ParameterValue. val is set, '\
                'but set to none'.format(__func_name))
            return False

        self.info = info
        self.value = val
        self.val_type = val_type
        self.secs = secs
        self.msecs = msecs
        return True


class SClient(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.stream = None
        self.register_config = None
        self.idle = MAX_CLIENT_IDLE

    def update_idle(self):
        self.idle = MAX_CLIENT_IDLE

    @coroutine
    def connect(self, config):
        __func_name = "SClient::connect"
        try:
            if not self.stream:
                self.stream = yield get_tcpclient().connect(self.host, self.port)
                self.stream.set_close_callback(self.close_callback)
            self.register_config = config
            data = self.register_config.serialize()
            data.mode = NetworkType.register_request
            yield self.stream.write(bytes(data.prepare()))
            self.update_idle()
            answer = yield NetworkUtils.read_data_structure(self.stream)
            self.update_idle()
            new_config = RegisterConfig()
            if new_config.deserialize(answer.data):
                print('{}(): New client registered: '.format(__func_name, new_config))
                self.register_config = new_config
                return True
            else:
                print('{}(): Error while converting new config'.format(__func_name))
            return False
        except StreamClosedError as err:
            print('{}(): stream is closed'.format(__func_name))
            return False

    def close_callback(self):
        __func_name = "SClient::close_callback"
        if not self.register_config: return
        id = self.register_config.id
        password = self.register_config.password
        if not get_client(id): return
        clients = get_global_clients()
        del clients[id]
        print('{}(): client with id {} was deleted'.format(__func_name, id))

class TMIClient(SClient):
    def __init__(self, host, port):
        super().__init__(host, port)
        self.tmiconfig = None

    @coroutine
    def connect(self, config):
        __func_name = "TMIClient::connect"
        self.tmiconfig = config
        reg_conf = RegisterConfig()
        data = config.serialize()
        data.mode = NetworkType.register_request
        reg_conf.data = data.data
        if (yield super().connect(reg_conf)):
            print('{}(): client is connected'.format(__func_name))
            print(self.register_config)
            return True
        else:
            print('{}(): client is not connected'.format(__func_name))
            return False

    @coroutine
    def add_parameters(self, params):
        __func_name = "TMIClient::add_parameters"
        req = ParameterInfo.serialize_list(params)
        req.mode = NetworkType.add_params
        yield self.stream.write(bytes(req.prepare()))
        self.update_idle()
        answer = yield NetworkUtils.read_data_structure(self.stream)
        self.update_idle()
        if answer.mode != NetworkType.added_params:
            return False
        lst = NetworkUtils.split_to_array(answer.data)
        infos = []
        for l in lst:
            tmp = NetworkUtils.split_to_array(l.data)
            if len(tmp) != 2:
                print('{}(): object in answer must have len 2, but is {}'.\
                    format(__func_name, len(tmp)))
                return False
            info = ParameterInfo()
            info.deserialize(tmp[0].data)
            flag = NetworkUtils.DecodeBool(tmp[1].data)
            infos.append((info, flag))
        return infos

    @coroutine
    def remove_parameters(self, params):
        req = ParameterInfo.serialize_list(params)
        req.mode = NetworkType.remove_params
        yield self.stream.write(bytes(req.prepare()))
        self.update_idle()

    @coroutine
    def request_parameters(self, params):
        req = ParameterInfo.serialize_list(params)
        req.mode = NetworkType.request_params
        yield self.stream.write(bytes(req.prepare()))
        self.update_idle()
        answer = yield NetworkUtils.read_data_structure(self.stream)
        self.update_idle()
        if answer.mode != NetworkType.params_values:
            return False
        lst = ParameterValue.deserialize_list(answer.data)
        return lst

class MainHandler(RequestHandler):
    def get(self):
        self.render('index.html')

class DataGetHandler(RequestHandler):
    def authorize_client(self, user_id, password):
        if user_id:
            if not password:
                self.write(json.dumps({'error': 'password was not specified'}))
                return None
            client = get_client(user_id)
            if client:
                if int(client.register_config.password) != int(password):
                    self.write(json.dumps({'error': 'incorrect password'}))
                    return None
            return client
        return None

    @coroutine
    def post(self):
        user_id = self.get_secure_cookie('user_id', None)
        password = self.get_secure_cookie('password', None)
        if self.get_argument('add_params', None):
            str_params = self.get_argument('add_params')
            # add new parameters
            params = json.loads(str_params)
            client = self.authorize_client(user_id, password)
            if not client:
                return
            tmp = []
            for p in params:
                tmp.append(ParameterInfo(name=p['name'], is_digital=p['is_digital']))
            params = yield client.add_parameters(tmp)
            res = []
            for param, flag in params:
                tmp = []
                tmp.append(param.tojson())
                tmp.append(flag)
                res.append(tmp)
            self.write(json.dumps({'response': res}))
            return
        if self.get_argument('register_request', None):
            str_params = self.get_argument('register_request')
            # register new client
            if user_id:
                if not password:
                    self.write(json.dumps({'error': 'password was not specified'}))
                    return
                client = get_client(user_id)
                if client:
                    if int(client.register_config.password) != int(password):
                        self.write(json.dumps({'error': 'incorrect password'}))
                        return
                    self.write(json.dumps({'response': {'id': client.register_config.id,
                        'password': client.register_config.password}}))
                    return
            config = TMIConfig(json.loads(str_params))
            client = TMIClient(get_tmihost(), get_tmiport())
            if not (yield client.connect(config)):
                self.write(json.dumps({'error': 'client was not connected'}))
                return
            if not add_client(client.register_config.id, client):
                self.write(json.dumps({'error': 'id {} already in using'\
                    .format(client.register_config.id)}))
                return
            self.set_secure_cookie('user_id', str(client.register_config.id))
            self.set_secure_cookie('password', str(client.register_config.password))
            self.write(json.dumps({'response':
                {'id': client.register_config.id,
                'password': client.register_config.password}
            }))
            return
        if self.get_argument('remove_params', None):
            str_params = self.get_argument('remove_params')
            params = json.loads(str_params)
            client = self.authorize_client(user_id, password)
            if not client:
                return
            tmp = []
            for p in params:
                tmp.append(ParameterInfo(name=p['name'], is_digital=p['is_digital']))
            yield client.remove_parameters(tmp)
            self.write(json.dumps({'response': 'ok'}))
            return
        if self.get_argument('request_params', None):
            str_params = self.get_argument('request_params')
            params = json.loads(str_params)
            client = self.authorize_client(user_id, password)
            if not client:
                return
            tmp = []
            for p in params:
                tmp.append(ParameterInfo(name=p['name'], is_digital=p['is_digital']))
            answer = yield client.request_parameters(tmp)
            if answer == False:
                self.write(json.dumps({'error': 'error during request_params'}))
                return
            res = [param.tojson() for param in answer]
            self.write(json.dumps({'response': res}))
            return
        self.write(json.dumps({'error': 'unknown operation'}))
        return

def clear_not_used_clients():
    clients = get_global_clients()
    for id, client in clients.items():
        client.idle -= CLEAR_CLIENTS_TIME
        if client.idle <= 0:
            client.stream.close()

def main():
    parser = argparse.ArgumentParser(description='Progressive web server')
    parser.add_argument('--port', type=int, default=8888,
        help='The port for the server to run on')
    parser.add_argument('--tmihost', type=str, required=True)
    parser.add_argument('--tmiport', type=int, default=13000)
    args = parser.parse_args()
    app = Application([
            (r'/', MainHandler),
            (r'/get_data/?', DataGetHandler),
        ],
        cookie_secret="SOME_COOKIE_SECRET",
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        debug=True
    )
    server = HTTPServer(app)
    server.listen(args.port)
    global global_tmi_host
    global global_tmi_port
    global_tmi_host = args.tmihost
    global_tmi_port = args.tmiport
    print('Server is listening on port', args.port)
    loop = IOLoop.current()
    clear_clients = PeriodicCallback(clear_not_used_clients, 1000 * CLEAR_CLIENTS_TIME)
    clear_clients.start()
    loop.start()

if __name__ == "__main__":
    main()